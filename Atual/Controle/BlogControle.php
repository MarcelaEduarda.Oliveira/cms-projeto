<?php 
    require_once("../Modelo/BlogModelo.php");
    require_once('../Modelo/ImgModelo.php');
    require_once("Conexao.php");
    class BlogControle{
        function inblog($blog){
            try{
                $conexao = new Conexao();

                $titulo = $blog->getTitulo();
                $autor = $blog->getAutor();
                $texto = $blog->getTexto();
                $email = $blog->getEmail();
                $cmd =  $conexao->getConexao()->prepare("INSERT INTO blog(titulo,autor,texto,email) VALUES(:titulo,:autor,:texto,:email);");
                $cmd->bindParam("titulo",$titulo);
                $cmd->bindParam("autor",$autor);
                $cmd->bindParam("texto", $texto);
                $cmd->bindParam("email", $email);
                if($cmd->execute()){
                    $ultimo_id = $conexao->getConexao()->lastInsertId();
                    $conexao->fecharConexao();
                    return $ultimo_id;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro PDO: {$e->getMessage()}";
                return false;

            }catch(Exception $e){
                echo"Erro no Banco: {$e->getMessage()}";
                return false;
            }
        }
        
        function selecionar($email){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT*from blog  where email=?;");
                $cmd->bindParam(1,$email);
                if($cmd->execute()){
                    $resultado=$cmd->fetchAll(PDO::FETCH_CLASS,"BlogModelo");
                    $conexao->fecharConexao();
                    return $resultado;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
           
        }

        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $sql= $conexao->getConexao()->prepare("SELECT * FROM blog"); 
                $sql->execute();
                $result = $sql->fetchAll(PDO:: FETCH_CLASS, "BlogModelo");
                return $result;       
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";

            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }

        function deletpost($id){
            try{
                $conexao = new Conexao();
                $com= $conexao->getConexao()->prepare("DELETE FROM blog WHERE id=:id");
                $com->bindParam("id",$id);
                if($com->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }

            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        function editar($blogg){
            try{
                $conexao = new Conexao();
                $blog = new BlogModelo();
                $id = $blogg->getId();
                $titulo = $blogg->getTitulo();
                $texto = $blogg->getTexto();
                $cmd = $conexao->getConexao()->prepare("UPDATE blog SET titulo=:titulo, texto=:texto WHERE id=:id");
                $cmd->bindParam("titulo", $titulo);
                $cmd->bindParam("id", $id);
                $cmd->bindParam("texto", $texto);
                if($cmd->execute()){
                        $conexao->fecharConexao();
                        return true;
                    }else{
                        $conexao->fecharConexao();
                        return false;
                    }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
    }
 ?>
