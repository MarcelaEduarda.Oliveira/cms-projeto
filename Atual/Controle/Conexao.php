<?php
    class Conexao{
        private $conexao;
        function __construct(){
            $host="localhost";
            $user="root";
            $bd="marcela";
            $pdw='';
            try{
                $this->setConexao(new PDO("mysql:host={$host};dbname={$bd}",$user,$pdw));
                $this->getConexao()->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            }catch(PDOException $e){
                echo "<p>Erro PDO: {$e->getMessage()}</p>";
            }catch(Exception $e){
                echo "<p>Erro geral: {$e->getMessage()}</p>";
            }
        }
        public function setConexao($data){
            $this->conexao=$data;
        }
        public function getConexao(){
            return $this->conexao;
        }
        public function fecharConexao(){
            $this->conexao=NULL;
        }
    }
?>
