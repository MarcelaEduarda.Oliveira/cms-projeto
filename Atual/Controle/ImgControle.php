<?php
	require_once("../Modelo/BlogModelo.php");
    require_once('../Modelo/ImgModelo.php');
	require_once("Conexao.php");
	class ImgControle{
		function inseririmg($imag){
			try{
				$conexao = new Conexao();
				$img = $imag->getNome();
				$tipo = $imag->getTipo();
				$dados = $imag->getDados();
				$post = $imag->getPost();
				$com = $conexao->getConexao()->prepare("INSERT INTO imagem(nome,tipo,dados,blog_id) VALUES(:nome, :tipo, :dados, :post);");
				$com->bindParam("nome", $img);
				$com->bindParam("tipo", $tipo); 
				$com->bindParam("dados",$dados);
				$com->bindParam("post",$post);
				if($com->execute()){
					$conexao->fecharConexao();
					return true;
				}else{
					$conexao->fecharConexao();
					return false;
				}
			}catch(PDOException $e){
				echo"Erro PDO: {$e->getMessage()}";
				return false;
			}catch(Exception $e){
				echo"Erro geral: {$e->getMessage()}";
				return false; 
			}
		}
		function select($id){
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("SELECT * FROM imagem WHERE id=:id");
			$cmd->bindParam("id", $id);
			$cmd->execute();
			$resultado = $cmd->fetch(PDO::FETCH_OBJ);
			return $resultado;
		}
		function select_img_blog($blog_id){
		    $conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("SELECT i.dados, i.id from imagem as i inner join blog as b on b.id =   i.blog_id where b.id=:id;");
			$cmd->bindParam("id", $blog_id);
			$cmd->execute();
			$resultado = $cmd->fetchAll(PDO::FETCH_OBJ);
			return $resultado;
		}
		function upimg($imagg){
			try{
				$conexao = new Conexao();

				$img = $imagg->getNome();
				$tipo = $imagg->getTipo();
				$dados = $imagg->getDados();
				$id = $imagg->getId();
				$cmd = $conexao->getConexao()->prepare("UPDATE imagem SET nome=:nome, tipo=:tipo, dados=:dados WHERE id=:id");
				$cmd->bindParam("id", $id);
				$cmd->bindParam("nome", $img);
				$cmd->bindParam("tipo", $tipo); 
				$cmd->bindParam("dados",$dados);
				if($cmd->execute()){
					$conexao->fecharConexao();
					return true;
				}else{
					$conexao->fecharConexao();
					return false;
				}
				
			}catch(PDOException $e){
				echo"Erro PDO: {$e->getMessage()} ";
			}
		}

		function deletimg($id){
            try{
                $conexao = new Conexao();
                $com= $conexao->getConexao()->prepare("DELETE FROM imagem WHERE blog_id=:id");
                $com->bindParam("id",$id);
                if($com->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }

            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
		}
		
		function veri_nome($nome){
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("SELECT nome from imagem where nome= :nome;");
			$cmd->bindParam("nome", $nome);
			if($cmd->execute() && $cmd->rowCount() == 1){
				return true;
			}else{
				return false;
			}

			
		}
		
	}

 ?>
