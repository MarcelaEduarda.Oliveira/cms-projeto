<?php 
require_once("../Modelo/UserModelo.php");
require_once("Conexao.php");
    class UserControle{
        function inserir($usuario){
            try{
                $conexao = new Conexao();
                $user = $usuario->getUser();
                $email = $usuario->getEmail();
                $senha = $usuario->getSenha();
                $foto = $usuario->getFoto();
                $tipo = $usuario->getTipo();
                $dados = $usuario->getDados();
                $cmd =  $conexao->getConexao()->prepare("INSERT INTO usuario(user,email,senha,foto,tipo,dados) VALUES(:user,:email,:senha,:foto,:tipo,:dados);");
                $cmd->bindParam("user",$user);
                $cmd->bindParam("email",$email);
                $cmd->bindParam("senha", $senha);
                $cmd->bindParam("foto", $foto);
                $cmd->bindParam("tipo", $tipo);
                $cmd->bindParam("dados", $dados);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                return false;

            }catch(Exception $e){
                return false;
            }
        }
        function verificar($email,$senha){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE email = :email AND senha =:senha");
                $cmd->bindParam("email", $email);
                $cmd->bindParam("senha", $senha);
                if($cmd->execute()){
                   if($cmd->rowCount()== 1){
                        return true;
                   }else{
                        return false;
                   }
                }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function vemail($email){
            $conexao = new Conexao();
            $sql = "SELECT*FROM usuario WHERE email=:email";
            $verificar= $conexao->getConexao()->prepare($sql);
            $verificar->bindParam("email", $email);
            if($verificar->rowCount()>0){
                $conexao->fecharConexao();
                return true;
            }else{
                $conexao->fecharConexao();
                return false;
            }
        } 
        function select($email){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT*from usuario  where email=?;");
                $cmd->bindParam(1,$email);
                if($cmd->execute()){
                    $resultado=$cmd->fetchAll(PDO::FETCH_CLASS,"UserModelo");
                    $conexao->fecharConexao();
                    return $resultado;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
           
        }
        function mostrar($id){
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE id=:id");
            $cmd->bindParam("id", $id);
            $cmd->execute();
            $resultado = $cmd->fetch(PDO::FETCH_OBJ);
            return $resultado;
        }
        function editar($use){
            try{
                $conexao = new Conexao();

                $id = $use->getId();
                $user = $use->getUser();
                $senha = $use->getSenha();
                $foto = $use->getFoto();
                $tipo = $use->getTipo();
                $dados = $use->getDados();
                $cmd = $conexao->getConexao()->prepare("UPDATE usuario SET user=:user, senha=:senha, foto=:foto, tipo=:tipo, dados=:dados WHERE id=:id");
                $cmd->bindParam("user", $user);
                $cmd->bindParam("id", $id);
                $cmd->bindParam("senha",$senha);
                $cmd->bindParam("foto", $foto);
                $cmd->bindParam("tipo", $tipo);
                $cmd->bindParam("dados", $dados);
                if($cmd->execute()){
                        $conexao->fecharConexao();
                        return true;
                    }else{
                        $conexao->fecharConexao();
                        return false;
                    }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        /*
        function vsenha($senha){
            $conexao = new Conexao();
            $sql ="SELECT*FROM usuario WHERE senha=:senha";
            $verificar = $conexao->getConexao()->prepare($sql);
            $verificar->bindParam("senha", $senha);
            if($verificar->rowCount()>0){
                $conexao->fecharConexao();
                return true;
            }else{
                $conexao->fecharConexao();
                return false;
            }
        }
		*/

        function update($usuarios){
            try{
                $conexao = new Conexao();

                $email = $usuarios->getEmail();
                $senha = $usuarios->getSenha();

                $cmd = $conexao->getConexao()->prepare("UPDATE usuario SET senha=:senha WHERE email=:email");
                $cmd->bindParam("email",$email);
                $cmd->bindParam("senha",$senha);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
     

    }
?>
