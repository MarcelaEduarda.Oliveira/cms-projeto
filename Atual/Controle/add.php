<?php
session_start();

require_once("UserControle.php");
require_once("../Modelo/UserModelo.php");
$usuario = new UserModelo();
$controle = new UserControle();
	
$usuario->setUser($_POST['user']);
$usuario->setEmail($_POST['email']);
$usuario->setSenha($_POST['senha']);
$imagem=$_FILES['foto'];
$usuario->setFoto($imagem['name']);
$usuario->setTipo($imagem['type']);
$usuario->setDados(file_get_contents($imagem['tmp_name']));

//Salvar o usuario
	
if($controle->inserir($usuario)){
	header("Location: ../Visual/login.php");
}else{
	echo "<script>alert('Este email já existe, coloque outro ou verifique se preencheu corretamene todos os campo');window.location.replace('../Visual/cadastro.php')</script>";
}

?>
