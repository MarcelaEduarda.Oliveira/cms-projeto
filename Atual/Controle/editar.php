<?php 
	try {
	    
		require_once("BlogControle.php");
		require_once("../Modelo/BlogModelo.php");
		
		$controle = new BlogControle();
		$blog = new BlogModelo();
		
		$blog->setId($_POST['id']);
		$blog->setTitulo($_POST['titulo']);
		$blog->setTexto($_POST['texto']);
		
		//Edita o post do usuário
		
		if($controle->editar($blog)){
		    
			header("Location: ../Visual/aduser.php");
		}
	} catch (Exception $e) {
		echo"Erro geral: {$e->getMessage()}";
	}

 ?>