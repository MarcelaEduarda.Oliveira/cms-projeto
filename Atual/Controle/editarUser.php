<?php 
	try {
	    
		require_once("UserControle.php");
		require_once("../Modelo/UserModelo.php");
		
		$controle = new UserControle();
		$user = new UserModelo();
		
		$user->setId($_POST['id']);
		$user->setUser($_POST['user']);
		$user->setSenha($_POST['senha']);
		$imagem=$_FILES['foto'];
		$user->setFoto($imagem['name']);
		$user->setTipo($imagem['type']);
		$user->setDados(file_get_contents($imagem['tmp_name']));

		//Edita o post do usuário
		
		if($controle->editar($user)){
		    
			header("Location: ../Visual/aduser.php");
		}
	} catch (Exception $e) {
		echo"Erro geral: {$e->getMessage()}";
	}

 ?>