<?php
	require_once("UserControle.php");
	
	session_start();
	
	$controle = new Usercontrole();
	
	$vrf = $controle->verificar($_POST['email'],$_POST['senha']);
	
	//Verifica se a sessão já existe e redireciona para a página de administração caso exista
	
	if(isset($_SESSION['user'])){
	    
		header("Location: ../Visual/aduser.php");
		
	}else{
	    //Caso a sessão não exista:
    	//Verifica se usuário está registrado  no banco e cria sessão
    	
    	if($vrf){
    		$_SESSION['user']=$_POST['email'];
    		$_SESSION['senha']=$_POST['senha'];
    		
    		//Redirecionamento para a página de administração 
    		
    		header("Location: ../Visual/aduser.php");
    		
    	}else{
    		echo "<script>alert('Senha, ou email incorreto.');window.location.replace('../visual/login.php')</script>";
    	}
	}
 ?>