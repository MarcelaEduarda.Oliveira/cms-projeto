<?php
	session_start();
	if(isset($_SESSION['user'])){
		try{
			require_once("../Modelo/ImgModelo.php");
			require_once("../Modelo/BlogModelo.php");
			require_once("ImgControle.php");
			require_once("BlogControle.php");

			$control  = new BlogControle();
			$controle = new ImgControle();
			$blog     = new BlogModelo();
			$imag     = new ImgModelo();
			

			$blog->setTitulo($_POST['titulo']);
			$blog->setAutor($_POST['autor']);
			$blog->setTexto($_POST['texto']);
			$blog->setEmail($_POST['email']);
			
			$imagem=$_FILES['img'];
			$contagem = count($imagem['name']);

			if(count($nomes) > 0){
				$_SESSION['erro'] = implode(", ", $nomes);
				header("Location: ../Visual/aduser.php");
				exit();
			}

			//Salvar blog e imagens
			if($contagem >= 2){
				$ultimo_id = $control->inBlog($blog);
				if($ultimo_id){
					for($i = 0; $i < $contagem; $i++){
						$imag->setNome($imagem['name'][$i]);
						$imag->setTipo($imagem['type'][$i]);
						$imag->setDados(file_get_contents($imagem['tmp_name'][$i]));
						$imag->setPost($ultimo_id);

						if($controle->inseririmg($imag)){
							header("Location: ../Visual/aduser.php");
						}			
					}
				}else{
					echo "<script>alert('Preencha corretamente todos os campos!!');window.location.replace('../visual/aduser.php')</script>";
				}
			}else{
				echo "<script>alert('Selecione no mínimo duas imagens!!');window.location.replace('../visual/aduser.php')</script>";
			}
		
		}catch(Exception $e){
			echo"Erro geral: {$e->Message()}";
		}
	}else{
		header("Location: index.php");
	}
 ?>

