<?php  
    session_start();
    if(isset($_SESSION['user'])){
        
        require_once("BlogControle.php");
        require_once("ImgControle.php");
        
        $controle = new BlogControle();
        $imgControl = new ImgControle();
        
        //Deleta a(s) imagem(ns) e o post do usuário
        
        if($imgControl->deletimg($_POST['id']) && $controle->deletpost($_POST['id'])){
            header("Location: ../Visual/aduser.php");
        }else{
            die();
        }
        
    }else{
        header("Location: index.php");
    }
?>
