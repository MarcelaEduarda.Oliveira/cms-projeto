<?php 
	class ImgModelo{
		private $id;
		private $nome;
		private $tipo;
		private $dados;
		private $post;

		public function setId($i){
			$this->id=$i;
		 }
		public function getId(){
			return $this->id;
		}
		public function setNome($no){
			$this->nome=$no;
		 }
		public function getNome(){
			return $this->nome;
		}
		public function setTipo($ti){
			$this->tipo=$ti;
		}
		public function getTipo(){
			return $this->tipo;
		}
		public function setDados($da){
			$this->dados = $da;
		}
		public function getDados(){
			return $this->dados;
		}
		public function setPost($p){
			$this->post = $p;
		}
		public function getPost(){
			return $this->post;
		}
		
	}

 ?>
