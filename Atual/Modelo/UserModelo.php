<?php 
	class UserModelo{
		private $id;
		private $user;
		private $email;
		private $senha;
		private $confsenha;
		private $foto;
		private $tipo;
		private $dados;


		//encapsulamento das variaveis
		public function setId($i){
			$this->id=$i;
		 }
		public function getId(){
			return $this->id;
		} 
		public function setUser($us){
			$this->user=$us;
		 }
		public function getUser(){
			return $this->user;
		} 
		public function setEmail($em){
			$this->email=$em;
		 }
		public function getEmail(){
			return $this->email;
		} 
		public function setSenha($sh){
			$this->senha=$sh;
		 }
		public function getSenha(){
			return $this->senha;
		} 
		public function setConfsenha($csh){
			$this->confsenha=$csh;
		 }
		public function getConfsenha(){
			return $this->confsenha;
		} 
		public function setFoto($f){
			$this->foto=$f;
		 }
		public function getFoto(){
			return $this->foto;
		}
		public function setTipo($t){
			$this->tipo=$t;
		 }
		public function getTipo(){
			return $this->tipo;
		} 
		public function setDados($d){
			$this->dados=$d;
		 }
		public function getDados(){
			return $this->dados;
		} 
	}
?>