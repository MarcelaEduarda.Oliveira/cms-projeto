<?php
	require_once("../Controle/BlogControle.php");
	require_once("../Controle/ImgControle.php");
	require_once("../Controle/UserControle.php");
	$controle = new BlogControle();
	$img_controle = new ImgControle();
	$user_controle = new UserControle();
	session_start();
if(isset($_SESSION['user'])){
	$email = $_SESSION['user'];
	$blog = $controle->selecionar($email);
	$user = $user_controle->select($email);
	echo "
		<!DOCTYPE html>
		<html>
		<head>
			<!-- Standard Meta -->
			<meta charset='utf-8' />
			<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
			<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

			<!-- Site Properties -->
			<title>Link | Innovation</title>
			<link rel='icon' href='img/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
			<link rel='stylesheet' type='text/css' href='css/semantic.css'>
			<link rel='stylesheet' type='text/css' href='components/icon.css'>
			<link rel='stylesheet' type='text/css' href='components/modal.min.css'>

			<style type='text/css'>
				body {
					background-color: #FFFFFF;
				}
				.ui.menu .item img.logo {
					margin-right: 1.5em;
				}
				.main.container {
					margin-top: 7em;
				}
				
				input[type='file'] {
			  		display: none
				}
				.in{
					margin-top: 10px;
				}
				
				.button{
					width: 100px;
				}
				.secondary.pointing.menu .toc.item {
      				display: none;
    			}

    			@media only screen and (max-width: 700px) {
      				.ui.fixed.menu {
        				display: none !important;
      				}
      				.secondary.pointing.menu .item,
      				.secondary.pointing.menu .menu {
       					 display: none;
      				}
      				.secondary.pointing.menu .toc.item {
        				display: block;
      				}
      				.masthead.segment {
       	 				min-height: 3px;
      				}
      				.masthead h1.ui.header {
        				font-size: 2em;
        				margin-top: 1.5em;
      				}
      				.masthead h2 {
        				margin-top: 0.5em;
        				font-size: 1.5em;
      				}
    			}
			</style>

		</head>
		<body>
	";
	foreach ($user as $val):
		echo"
			<!-- menu flutuante -->
			<div class='ui large top fixed hidden menu'>
  				<div class='ui container'>
  					<img class='ui top aligned tiny image'  src='../Controle/mostrar.php?id={$val->getId()} style='margin-right: 2px;'>
    				<a class='item' href='../index.php' >{$val->getUser()}</a>
    				<div class='right menu'>
						<a class='item' href='conta.php?id={$val->getId()}'>Conta</a>
						<a class='item' href='blog.php'>Blog</a>
						<a class='item' href='../Controle/sair.php'>Sair</a>
    				</div>
  				</div>
			</div>
			<!-- menu mobile -->
			<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
				<a class='item' href='conta.php?id={$val->getId()}'>Conta</a>
				<a class='item' href='blog.php'>Blog</a>
  				<a class='item' href='../Controle/sair.php'>Sair</a>
			</div>	
			<div class='pusher'>  <!-- inicio menu pusher -->
				<div class='ui masthead'>
					<div>
						<div class='ui large secondary pointing menu'>
							<a class='toc item'>
								<i class='sidebar icon' style='color: black;'></i>
							</a>
							<img style='margin-left: 20px; margin-right: 5px;' class='ui top aligned tiny image' src='../Controle/mostrar.php?id={$val->getId()} >
    						<a class='item' href='../index.php' >{$val->getUser()}</a>
							<div class='right menu'>
								<a class='item' href='conta.php?id={$val->getId()}'>Conta</a>
								<a class='item' href='blog.php'>Blog</a>
								<a class='item' href='../Controle/sair.php'>Sair</a>
							</div>
						</div>
					</div>
				</div>
		";
	endforeach;
	foreach ($blog as $value):
		$qtdImg = count($img_controle->select_img_blog($value->getId()));
 		echo"
			<div class='ui main text container'>
				<center><h1 class='ui header'>{$value->getTitulo()}</h1></center>
			</div>
			<br/><br/>

			<div class='ui text container'>
				<div class='ui three column grid computer and tablet only'>
					<div class='column'>
					</div>

					<!--inicio blog computador/tablet-->
					<div class='column'>
						<div class='ui fluid card'>
    						<!-- autor computador -->
							<div class='image'>";
								if($qtdImg != 0){
							    	echo "<img src='../Controle/teste.php?id={$img_controle->select_img_blog($value->getId())[0]->id}'>";
								}else{
							   		echo "<img src='img/indisponivel.png'>";
								}
								echo "
							</div>
							<div class='content'>
								<p style='text-align: center;' class='header'>{$value->getAutor()}</p>
							</div>
							<a href='editimg.php?id={$img_controle->select_img_blog($value->getId())[0]->id}'><i class='edit icon'></i></a>
						</div>
					</div>

					<div class='column'>
					</div>
				</div>";
				echo "
					<!--inicio blog mobile-->
					<div class='ui column grid mobile only'>
						<div class='column'>
							<div class='ui fluid card'>
				";
				if($qtdImg != 0){
					echo "<img style='height: 320px;' src='../Controle/teste.php?id={$img_controle->select_img_blog($value->getId())[0]->id}'>
						<div class='content'>
								<p style='text-align: center;' class='header'>{$value->getAutor()}</p>
							</div>
					<a href='editimg.php?id={$img_controle->select_img_blog($value->getId())[0]->id}'><i class='edit icon'></i></a>
					";

				}else{
			        echo "<img src='img/indisponivel.png'>";
				}	
				echo"

							</div>
						</div>
					</div>
			

			<br/><br/>
			";
			if($qtdImg != 0){
				for($i = 1; $i < $qtdImg; $i++){
				    echo "
				        <div class='ui text container'>
				        	<img src='../Controle/teste.php?id={$img_controle->select_img_blog($value->getId())[$i]->id}' width='200' height='200' class='ui three column grid computer and tablet only'>
				        	<center><img src='../Controle/teste.php?id={$img_controle->select_img_blog($value->getId())[$i]->id}' width='200' height='200' class='ui column grid mobile only'></center>
				        	<br>
				        	<a href='editImg.php?id={$img_controle->select_img_blog($value->getId())[$i]->id}'><i class='edit icon'></i></a>
				    ";
				}
			}else{
			    echo "<div class='ui text container'><img src='img/indisponivel.png'>";
			}
	    
	    	echo "
					
			<center><h1 class='ui three column grid computer and tablet only' style='margin-left: 240px; margin-top: -200px;'>{$value->getTexto()}</h1></center>
			<center><br /><br /><h1 class='ui column grid mobile only' style='margin-left: 110px;'>{$value->getTexto()}</h1></center>
			</div>
			<br /><br />
			<center>
				<a href='remover.php?id={$value->getId()}'><i class='trash icon'></i></a>                       
				<a href='editPost.php?id={$value->getId()}'><i class='edit icon'></i></a>
			</center>
			<br /><br /><br /><br />
			</div>
		";

	    

	endforeach;
	foreach ($user as $valu):
		echo"
			<br /><br /><br /><br />
			<div class='ui container'>
				<form class='ui form' action='../Controle/recebe.php' id='tab' method='post' enctype='multipart/form-data'>
					<input type='hidden' name='email' value='{$email}' />
					<input type='hidden' name='autor' value='{$valu->getUser()}' />
					<h4 class='ui dividing header' id='lb'>Faça um novo Post</h4>
					<div class='field'>
						<div class='three fields'>
							<div class='field'>
								<input type='text' name='titulo' class='in' value='Titulo' required>
							</div>
							<div class='ui left icon input field'>
								<textarea rows='2' name='texto' class='in' placeholder='texto' required></textarea>
							</div>
						</div>
					</div>				
					<div class='field'>
						<label for='img'><i class='image icon'></i><a>Selecionar Imagens: Autor(a),e uma, ou mais do post.</a></label>
						<div class='ui left icon input'>
							<input type='file' id='img' name='img[]' multiple >
						</div>
						<div>
							<input type='submit' class='ui fluid large submit button' style='background-color: #080b34; width: 100px; color: white;' value='Enviar' />
						</div>
						<br />
						<p>Caso houver erro entre em contato:  marcelaeduarda678.me@gmail.com</p>
					</div>
				</form>
				<br />
				<br /><br />
			</div>
		</div><!-- fim menu pusher -->
		";
	endforeach;
include_once ("nichos/scripts.php"); 
}else{
	header("Location: login.php");
}

?>
	</body>
</html>

	
