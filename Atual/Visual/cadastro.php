<?php include_once "nichos/head.php"; ?> <!--cabeçalho-->
<link rel='stylesheet' type='text/css' href='css/visual.css'>
</head>
<body class="body">


	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='../index.php'>Home</a>
		<a class='item' href='login.php'>Login</a>
		<a class='item' href='sobre.php'>Sobre</a>
	</div>



	<div class='ui large secondary pointing menu' style='background-color: white;'>
		<a class='toc item'>
			<i class='sidebar icon' style='color: black;'></i>
		</a>
		<a class='active item' href='../index.php'>Home</a>
		<div class='right menu'>
			<a class='item' href='login.php'>Login</a>
			<a class='item' href='sobre.php'>Sobre</a>
		</div>
	</div>
</div>


<br><br><br><br>

<div class='ui middle aligned center aligned grid'>
	<div class='column'>
		<h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
			Crie sua conta
		</h1>

		<?php include_once "nichos/formcadastro.php"; ?> <!--formulario-->

		<div class='ui message'>
			Já tem uma conta? <a href='login.php'> Clique aqui</a>
		</div>
	</div>
</div>

</body>
<?php include_once "nichos/scripts.php"; ?>
</html>