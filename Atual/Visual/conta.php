<?php 
	include_once "nichos/head.php"; 
	echo"<!--cabeçalho-->
		<link rel='stylesheet' type='text/css' href='css/visual.css'>
		</head>
	";
	require_once("../Controle/UserControle.php");
	$control = new UserControle();
	session_start();
	if(isset($_SESSION['user'])){
		$email = $_SESSION['user'];
		$user = $control->select($email);
			echo"
				<style type='text/css'>
    				body > .grid {
      					height: 100%;
    				}
    				.image {
      					margin-top: -100px;
    				}
    				.column {
      					max-width: 450px;
    				}
  				</style>
  				<body>
  			";
  			foreach ($user as $value):
  				echo"
  					<div class='ui middle aligned center aligned grid'>
  						<div class='column'>
    						<h2 class='ui teal image header'>
      							<img src='img/icon.png' class='image'>
      							<div class='content' style='color: #080b34;'>
        							Editar conta
      							</div>
    						</h2>
    						<form class='ui large form' action='../Controle/editarUser.php' method='post' enctype='multipart/form-data'>
    						<input type='hidden' name='id' value='{$value->getId()}' />
								<div class='ui stacked segment'>
									<div class='field'>
										<div class='ui left icon input'>
											<i class='user icon'></i>
											<input type='text' name='user' value='{$value->getUser()}'>
										</div>
									</div>
									<div class='field'>
										<div class='ui left icon input'>
											<i class='lock icon'></i>
											<input type='password' name='senha' placeholder='{$value->getSenha()}' id='senha'>
										</div>
									</div>
									<div class='field'>
										<div class='ui left icon input'>
											<i class='lock icon'></i>
											<input type='password' name='confsenha' placeholder='Confirmar senha' id='confsenha'>
										</div>
										<br  /><br  />
										<div>
											<label for='foto'><i class='image icon'></i><a>Selecionar imagem de usuário</a></label>
											<div class='ui left icon input'>
												<input type='file' id='foto' name='foto'>
											</div>
										</div>
										<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Editar conta' />
									</div>
								</div>
								<div class='ui error message'></div>
							</form>
							<div class='ui message'>
							Deseja voltar? <a href='aduser.php'> Clique aqui</a>
						</div>	
    					</div>
					</div>
  				</body>
			";
		endforeach;
		include_once ("nichos/scripts.php"); 
	}else{
		header("Location: login.php");
	}
?>