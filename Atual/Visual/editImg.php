<?php 
	session_start();
	if(isset($_SESSION['user'])){
		echo"
		<!DOCTYPE html>
		<html>
			<head>
				<!-- Standard Meta -->
				<meta charset='utf-8' />
				<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
				<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
				<!-- Site Properties -->
				<title>Link | Innovation</title>
				<link rel='icon' href='img/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
				<link rel='stylesheet' type='text/css' href='css/semantic.css'>
				<link rel='stylesheet' type='text/css' href='components/icon.css'>
				<link rel='stylesheet' type='text/css' href='components/modal.min.css'>
			</head>
			<style type='text/css'>
				input[type='file'] {
			  		display: none
				}

			</style>
			<body>
			<br /><br /><br /><br /><br /><br />
			<center>
				<div class='ui container'>
				<center>
					<div class='ui message' style='width: 300px;'>
			
 			 			<div class='header'>
    						Editar Imagem
  						</div>
  					
 						<div class='ui   quote '>
							<div class='ui equal width stackable internally celled grid'>
								<div class='center aligned row'>
									<div class='column'>
 										<form class='ui  form' action='../Controle/upimg.php' method='post' enctype='multipart/form-data'>		
 											<div class='field'>
												<input type='hidden' name='id' value='{$_GET['id']}'>
												<label for='img'><i class='image icon'></i><a>Selecionar imagem</a></label>
												<div class='ui left icon input'>
													<input type='file' id='img' name='teste'>
												</div>
											<div>
											<center>
											<input type='submit' class='ui fluid large submit button' style='background-color: #080b34; width: 100px; color: white;' value='Editar' />
											</center>
    									</form>
    								</div>
	   							</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</center>
		</body>
		";
	}else{
		header("Location: login.php");
	}
?>