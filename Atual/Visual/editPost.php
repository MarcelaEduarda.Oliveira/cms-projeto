<?php 
	include_once "nichos/head.php"; 
	echo"<!--cabeçalho-->
		<link rel='stylesheet' type='text/css' href='css/visual.css'>
		</head>
	";
	require_once("../Controle/BlogControle.php");
	$control = new BlogControle();
	session_start();
	if(isset($_SESSION['user'])){
		$email = $_SESSION['user'];
		$post = $control->selecionar($email);
			echo"
				<style type='text/css'>
    				body > .grid {
      					height: 100%;
    				}
    				.image {
      					margin-top: -100px;
    				}
    				.column {
      					max-width: 450px;
    				}
  				</style>
  				<body>
  			";
  			foreach ($post as $value):
  				echo"
  					<div class='ui middle aligned center aligned grid'>
  						<div class='column'>
    						<h2 class='ui teal image header'>
      							<img src='img/icon.png' class='image'>
      							<div class='content' style='color: #080b34;'>
        							Editar Post
      							</div>
    						</h2>
    						<form class='ui large form' action='../Controle/editar.php' method='post'>
    							<input type='hidden' name='id' value='{$_GET['id']}' />
								<div class='ui stacked segment'>
									<div class='field'>
										<div class='ui left icon input'>
											<input type='text' name='titulo' value='{$value->getTitulo()}'>
										</div>
									</div>
									<div class='field'>
										<div class='ui left icon input'>
											<textarea  name='texto' class='in' placeholder='{$value->getTexto()}' required></textarea>
										</div>
									</div>
									<br />
									<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Editar post ' />
										<br  /><br  />
								</div>
								<div class='ui error message'></div>
							</form>
							<div class='ui message'>
							Deseja voltar? <a href='aduser.php'> Clique aqui</a>
						</div>	
    					</div>
					</div>
  				</body>
				";
			endforeach;
		include_once ("nichos/scripts.php"); 
	}else{
		header("Location: login.php");
	}
?>