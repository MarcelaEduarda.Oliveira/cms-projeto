<div class='ui inverted vertical footer segment ' style='background-color: #080b34;'>
	<div class='ui container'>
		<div class='ui stackable inverted divided equal height stackable grid'>
			<div class='three wide column'>
				<h4 class='ui inverted header'>Links</h4>
				<div class='ui inverted link list'>
					<a href='index.php' class='item'>Home</a>
					<a href='login.php' class='item'>Login</a>
					<a href='cadastrar.php' class='item'>Cadastre-se</a>
					<a href='sobre.php' class='item'>Sobre</a>
				</div>
			</div>
			<div class='three wide column'>
				<h4 class='ui inverted header'>Redes Socias</h4>
				<div class='ui inverted link list'>
					<a href='#' class='item'><i class='facebook icon'></i> Facebook</a>
					<a href='#' class='item'><i class='instagram icon'></i> Instagram</a>
					<a href='#' class='item'><i class='twitter icon'></i> Twitter</a>
				</div>
			</div>
			<div class='seven wide column'>
				<img src='img/icon.png' class='ui small image'>
				<h4 class='ui inverted header'>Link Innovation</h4>
				<p>O ideal para atingir novos horizontes.</p>
			</div>
		</div>
	</div>
</div>