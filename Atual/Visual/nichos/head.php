<!DOCTYPE html>
<html>
<head>
	<!-- Standard Meta -->
	<meta charset='utf-8' />
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

	<!-- Site Properties -->
	<title>Link | Innovation</title>
	<link rel='icon' href='img/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
	<link rel='stylesheet' type='text/css' href='css/semantic.css'>
	<link rel='stylesheet' type='text/css' href='css/custom.css'>
	<link rel='stylesheet' type='text/css' href='components/icon.css'>