
<!-- menu flutuante -->
<div class='ui large top fixed hidden menu'>
	<div class='ui container'>
		<a class='active item' href='../index.php'>Home</a>
		<div class='right menu'>
			<a class='item' href='login.php'>Login</a>
			<a class='item' href='cadastro.php'>Cadastre-se</a>
			<a class='item' href='sobre.php'>Sobre</a>
			<a class='item' href='blog.php'>Blog</a>
		</div>
	</div>
</div>

<!-- menu mobile -->
<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
	<a class='active item' href='../index.php'>Home</a>
	<a class='item' href='login.php'>Login</a>
	<a class='item' href='cadastro.php'>Cadastre-se</a>
	<a class='item' href='sobre.php'>Sobre</a>
	<a class='item' href='blog.php'>Blog</a>
</div>


<!-- menu principal -->
<div class='pusher'>  <!-- inicio menu pusher -->

	<div class='ui masthead'>

		<div>
			<div class='ui large secondary pointing menu'>
				<a class='toc item'>
					<i class='sidebar icon' style='color: black;'></i>
				</a>
				<a class='active item' href='../index.php'>Home</a>
				<div class='right menu'>
					<a class='item' href='login.php'>Login</a>
					<a class='item' href='cadastro.php'>Cadastre-se</a>
					<a class='item' href='sobre.php'>Sobre</a>
					<a class='item' href='blog.php'>Blog</a>
				</div>
			</div>
		</div>
	</div>