<?php
	session_start();
	if(isset($_SESSION['user'])){
		echo"
			<!DOCTYPE html>
			<html>
				<head>
					<!-- Standard Meta -->
					<meta charset='utf-8' />
					<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
					<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

					<!-- Site Properties -->
					<title>Link | Innovation</title>
					<link rel='icon' href='img/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
					<link rel='stylesheet' type='text/css' href='css/semantic.css'>
					<link rel='stylesheet' type='text/css' href='components/icon.css'>
					<link rel='stylesheet' type='text/css' href='components/modal.min.css'>
				</head>
				<body>
					<br /><br /><br /><br /><br /><br />
					<center>
						<div class='ui container'>
							<div class='ui negative message' style='width: 400px;'>
  								<i class='close icon'></i>
 			 					<div class='header'>
    								Confirmar Remoção
  								</div>
 								<p>Tem certeza que deseja remover esse post?</p>
 								<div class='ui vertical  quote '>
									<div class='ui equal width stackable internally celled grid'>
											<div class='center aligned row'>
												<div class='column'>
 													<form class='ui  form' action='aduser.php' method='post'>			   					
    		   											<i class='checkmark icon' style='color: green;'></i>
    		   											<input class='ui green basic cancel button' type='submit' id='não' value='Não' style='background-color: white;color: green; font-size: 15px;'>
    												</form>
    											</div>
												<div class='column'>				
    												<form class='ui  form' action='../Controle/remover.php' method='post'>
    		   											<i class='remove icon' style='color: red;'></i>
    		   											<input type='hidden' name='id' value='{$_GET['id']}'>
    		   											<input class='ui red basic cancel  button' type='submit' value='Sim' style=' background-color: white;color: red; font-size: 15px;'>
    												</form>
	   											</div>
	   										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</center>
				</body>
		";
	}else{
		header("Location: login.php");
	}
?>