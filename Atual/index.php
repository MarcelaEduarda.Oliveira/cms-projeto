
<!DOCTYPE html>
<html>
<head>
	<!-- Standard Meta -->
	<meta charset='utf-8' />
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

	<!-- Site Properties -->
	<title>Link | Innovation</title>
	<link rel='icon' href='Visual/img/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
	<link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
	<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
</head>
<body>	
	<!-- menu flutuante -->
	<div class='ui large top fixed hidden menu'>
		<div class='ui container'>
			<a class='active item' href='index.php'>Home</a>
			<div class='right menu'>
				<a class='item' href='Visual/login.php'>Login</a>
				<a class='item' href='Visual/cadastro.php'>Cadastre-se</a>
				<a class='item' href='Visual/sobre.php'>Sobre</a>
				<a class='item' href='Visual/blog.php'>Blog</a>
			</div>
		</div>
	</div>

	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='index.php'>Home</a>
		<a class='item' href='Visual/login.php'>Login</a>
		<a class='item' href='Visual/cadastro.php'>Cadastre-se</a>
		<a class='item' href='Visual/sobre.php'>Sobre</a>
		<a class='item' href='Visual/blog.php'>Blog</a>
	</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='index.php'>Home</a>
					<div class='right menu'>
						<a class='item' href='Visual/login.php'>Login</a>
						<a class='item' href='Visual/cadastro.php'>Cadastre-se</a>
						<a class='item' href='Visual/sobre.php'>Sobre</a>
						<a class='item' href='Visual/blog.php'>Blog</a>
					</div>
				</div>
			</div>
		</div>
	<header>
		<img class='ui fluid image' src='Visual/img/banner.png'>
	</header>

	<!-- inicioconteudo -->

	<div class='ui vertical stripe segment'>
		<div class='ui middle aligned stackable grid container'>
			<div class='row'>
				<div class='eight wide column'>
					<h3 class='ui header'>Compartilhe suas idéias.</h3>
					<p>O Link Innovation garante que você tenha tudo que precisa para compartilhar suas idéias  hoje, pois possui um suporte de primeira classe, ferramentas de ajuda e muito mais.</p>
					<h3 class='ui header'>Crie qualquer tipo de post, para qualquer objetivo.</h3>
					<p>

						Com o Link Innovation, você cria posts que atende às suas necessidades. Com a sua otimização integrada e temas responsivos compatíveis com dispositivos móveis, não há limites para o que você pode alcançar com essa plataforma.
					</p>
				</div>
				<div class='six wide right floated column'>
					<img src='Visual/img/img1.jpg' class='ui large bordered rounded image'>
				</div>
			</div>
			<div class='row'>
				<div class='center aligned column'>
					<a class='ui huge button' href='Visual/cadastro.php' style='background-color: #080b34;color: white;'>Começar</a>
				</div>
			</div>
		</div>
	</div>


	<div class='ui vertical stripe quote segment'>
		<div class='ui equal width stackable internally celled grid'>
			<div class='center aligned row'>
				<div class='column'>
					<h3>Editor intuitivo</h3>
					<p>Nosso editor é rápido e intuitivo.</p>
				</div>
				<div class='column'>
					<h3>Blogs para celulares e computadores</h3>
					<p>
						Atualize seus posts em celulares ou em computadores: iOS, Android, Mac, Windows, e Linux.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class='ui vertical stripe segment'>
		<div class='ui text container'>
			<h3 class='ui header'>As pessoas amam o Link Innovation!</h3>

			<div class='ui comments'>
				<h4 class='ui dividing header'>Alguns comentários</h4>
				<div class='comment'>
					<a class='avatar'>
						<img src='Visual/img/av1.png'>
					</a>
					<div class='content'>
						<a class='author'>Matt</a>
						<div class='metadata'>
							<span class='date'>Hoje às 17:00</span>
						</div>
						<div class='text'>Que artístico! </div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
				</div>
				<div class='comment'>
					<a class='avatar'>
						<img src='Visual/img/av2.png'>
					</a>
					<div class='content'>
						<a class='author'>Mari</a>
						<div class='metadata'>
							<span class='date'>Ontem às 13:30</span>
						</div>
						<div class='text'>
							<p>Isto foi muito útil para a minha pesquisa. Muito obrigada!</p>
						</div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
					<div class='comments'>
						<div class='comment'>
							<a class='avatar'>
								<img src='Visual/img/av1.png'>
							</a>
							<div class='content'>
								<a class='author'>Marcos Silva</a>
								<div class='metadata'>
									<span class='date'>Agora</span>
								</div>
								<div class='text'>Mari, concordo com você!! :) </div>
								<div class='actions'>
									<a class='Resposta'>Resposta</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='comment'>
					<a class='avatar'>
						<img src='Visual/img/av2.png'>
					</a>
					<div class='content'>
						<a class='author'>Nay Maria</a>
						<div class='metadata'>
							<span class='date'>5 dias atrás</span>
						</div>
						<div class='text'>Cara, isso foi impressionante. Gostei muito! <3</div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
				</div>
			</div>

			<h4 class='ui horizontal header divider'>
				<a href='Visual/login.php'>Crie já o seu!</a>
			</h4>
			<h3 class='ui header'>Faça posts com segurança.</h3>
			<p>Com o Link Innovation, você tem muitas possibilidades de compartilhar seus posts de forma segura e intuitiva, além de ser gratuito. Então, o que você está esperando?</p>
			<center ><img src='Visual/img/icon.png' class='ui medium image'></center><br>

			<center><a class='ui large button' href='Visual/cadastro.php' style='background-color: #080b34;color: white;'>Venha conferir!</a></center>
		</div>
	</div>


	<!-- fimconteudo -->
	<div class='ui inverted vertical footer segment ' style='background-color: #080b34;'>
	<div class='ui container'>
		<div class='ui stackable inverted divided equal height stackable grid'>
			<div class='three wide column'>
				<h4 class='ui inverted header'>Links</h4>
				<div class='ui inverted link list'>
					<a href='index.php' class='item'>Home</a>
					<a href='Visual/login.php' class='item'>Login</a>
					<a href='Visual/cadastro.php' class='item'>Cadastre-se</a>
					<a href='Visual/sobre.php' class='item'>Sobre</a>
				</div>
			</div>
			<div class='three wide column'>
				<h4 class='ui inverted header'>Redes Socias</h4>
				<div class='ui inverted link list'>
					<a href='#' class='item'><i class='facebook icon'></i> Facebook</a>
					<a href='#' class='item'><i class='instagram icon'></i> Instagram</a>
					<a href='#' class='item'><i class='twitter icon'></i> Twitter</a>
				</div>
			</div>
			<div class='seven wide column'>
				<img src='Visual/img/icon.png' class='ui small image'>
				<h4 class='ui inverted header'>Link Innovation</h4>
				<p>O ideal para atingir novos horizontes.</p>
			</div>
		</div>
	</div>


</div> <!-- fim menu pusher -->
</body>
<script src='Visual/js/jquery.min.js'></script>
<script src='Visual/js/semantic.js'></script>
<script src='Visual/components/visibility.js'></script>
<script src='Visual/components/sidebar.js'></script>
<script src='Visual/components/transition.js'></script>
<script src='Visual/components/modal.min.js'></script>	
<script src='Visual/js/teste.js'></script>
</html>