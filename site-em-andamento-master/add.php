<?php
try{
	require_once("modelo/Usermodelo.php");
	require_once("controle/Usercontrole.php");
	
	$usuario = new Usermodelo();
	$controle = new Usercontrole();
	
	$usuario->setUser($_POST['user']);
	$usuario->setEmail($_POST['email']);
	$usuario->setSenha($_POST['senha']);
	
	//Salvar o usuario
	
	if($controle->inserir($usuario)){
		header("Location: login.php");
	}
	
}catch(Exception $e){
	echo "Erro: $e->getMessage()";
}
?>
