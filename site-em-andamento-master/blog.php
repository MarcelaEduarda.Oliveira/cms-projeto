<?php
	require_once("controle/Blogcontrole.php");
	require_once("controle/Imgcontrole.php");
	$controle = new Blogcontrole();
	$img_controle = new Imgcontrole();
	$blog = $controle->selecionar();
	


	echo "
		<!DOCTYPE html>
		<html>
		<head>
			<!-- Standard Meta -->
			<meta charset='utf-8' />
			<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
			<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

			<!-- Site Properties -->
			<title>Link | Innovation</title>
			<link rel='icon' href='img/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
			<link rel='stylesheet' type='text/css' href='css/semantic.css'>
			<link rel='stylesheet' type='text/css' href='components/icon.css'>
			<link rel='stylesheet' type='text/css' href='components/modal.min.css'>

			<style type='text/css'>
				body {
					background-color: #FFFFFF;
				}
				.ui.menu .item img.logo {
					margin-right: 1.5em;
				}
				.main.container {
					margin-top: 7em;
				}
				
				input[type='file'] {
			  		display: none
				}
				.in{
					margin-top: 10px;
				}
				
				.button{
					width: 100px;
				}

			</style>

		</head>
		<body>
				<div class='ui fixed inverted  menu'>
					<div class='ui container'>
						<a href='#' class='header item'>
							<img class='logo' src='img/icon.png'>
							Blog Link Innovation
						</a>
						<div class='right menu'>
							<a class='item' href='index.php'><i class='arrow right icon'></i> Sair</a>
						</div>
					</div>
				</div>";
 foreach ($blog as $value):
	$qtdImg = count($img_controle->select_img_blog($value->getId()));
 	echo"
			<div class='ui main text container'>
				<center><h1 class='ui header'>{$value->getTitulo()}</h1></center>
			</div>
			<br/><br/>


			<div class='ui text container'>

				<div class='ui three column grid computer and tablet only'>

					<div class='column'>
					</div>

					<!--inicio blog computador/tablet-->
					<div class='column'>
						<div class='ui fluid card'>
    <!-- autor computador -->
							<div class='image'>";
							if($qtdImg != 0){
							    echo "<img src='teste.php?id={$img_controle->select_img_blog($value->getId())[0]->id}'>";
							}else{
							    echo "<img src='img/indisponivel.png'>";
							}
							echo "
							</div>

							<div class='content'>
								<p style='text-align: center;' class='header'>{$value->getAutor()}</p>
							</div>
						</div>
					</div>


					<div class='column'>
					</div>
				</div>";
				echo "
					<!--inicio blog mobile-->
					<div class='ui column grid mobile only'>
						<div class='column'>
						<div class='ui fluid card'>
						";
				if($qtdImg != 0){
					 echo "<img src='teste.php?id={$img_controle->select_img_blog($value->getId())[0]->id}'>";
				}else{
			        echo "<img src='img/indisponivel.png'>";
				}	
				echo"
					<div class='content'>
						<p style='text-align: center;' class='header'>{$value->getAutor()}</p>
					</div>
				</div>
				</div>
			</div>

			</div>

			<br/><br/>
				   ";
				if($qtdImg != 0){
				    for($i = 1; $i < $qtdImg; $i++){
				        echo "
				        <div class='ui text container'>
				        <img src='teste.php?id={$img_controle->select_img_blog($value->getId())[$i]->id}' width='200' height='200'>
				        ";
				    }
				}else{
				    echo "<div class='ui text container'><img src='img/indisponivel.png'>";
				}
	    
	    echo "
					
				<p style='text-align: center;'>{$value->getTexto()}</p>
			</div>
			
			<br /><br /><br /><br />
		";

	    

	endforeach;
	echo"
	<br /><br />
	<br /><br />
		<div class='ui container'>
			<center>
					<h3>Mude a cor do menu e do footer!</h3>
					<button class='ui red basic button' onclick='red()'>Vermelho</button>
					<button class='ui orange basic button' onclick='orange()'>Laranja</button>
					<button class='ui yellow basic button' onclick='yellow()'>Amarelo</button>
					<button class='ui green basic button' onclick='green()'>Verde</button>
					<button class='ui teal basic button' onclick='teal()'>Cerceta</button>
					<button class='ui blue basic button' onclick='blue()'>Azul</button>
					<button class='ui violet basic button' onclick='violet()'>Violeta</button>
					<button class='ui purple basic button' onclick='purple()'>Roxo</button>
					<button class='ui pink basic button' onclick='pink()'>Rosa</button>
					<button class='ui brown basic button' onclick='brown()'>Marron</button>
					<button class='ui grey basic button' onclick='grey()'>Cinza</button>
					<button class='ui black basic button' onclick='black()'>Preto</button>
			</center>
			</div>
			<style type='text/css'>
				footer{
					margin-top: 70px;
					background: #1b1c1d;
					width: 100%;
					height: 52.6px;
					bottom: 0;
					left: 0;
				}
			</style>
			<footer class='div'>
			</footer>
			";

include_once ("nichos/scripts.php"); 

?>
<script type="text/javascript">
	function red(){
        document.querySelector('.div').style.background = 'Red';
        document.querySelector('.menu').style.background = 'Red';
    }
    function orange(){
        document.querySelector('.div').style.background = 'Orange';
        document.querySelector('.menu').style.background = 'Orange';
    }
    function yellow(){
        document.querySelector('.div').style.background = 'Yellow';
        document.querySelector('.menu').style.background = 'Yellow';
    }
    function green(){
        document.querySelector('.div').style.background = 'Green';
        document.querySelector('.menu').style.background = 'Green';
    }
    function teal(){
        document.querySelector('.div').style.background = 'Teal';
        document.querySelector('.menu').style.background = 'Teal';
    }
    function blue(){
        document.querySelector('.div').style.background = 'Blue';
        document.querySelector('.menu').style.background = 'Blue';
    }
    function violet(){
        document.querySelector('.div').style.background = 'Violet';
        document.querySelector('.menu').style.background = 'Violet';
    }
    function purple(){
        document.querySelector('.div').style.background = 'Purple';
        document.querySelector('.menu').style.background = 'Purple';
    }
    function pink(){
        document.querySelector('.div').style.background = 'Pink';
        document.querySelector('.menu').style.background = 'Pink';
    }
    function brown(){
        document.querySelector('.div').style.background = 'Brown';
        document.querySelector('.menu').style.background = 'Brown';
    }
    function grey(){
        document.querySelector('.div').style.background = 'Grey';
        document.querySelector('.menu').style.background = 'Grey';
    }
    function black(){
        document.querySelector('.div').style.background = 'Black';
        document.querySelector('.menu').style.background = 'Black';
    }

</script>

	</body>
</html>

	
