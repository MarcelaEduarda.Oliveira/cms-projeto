<?php 
	require_once("modelo/Blogmodelo.php");
    require_once('modelo/Imgmodelo.php');
	require_once("Conexao.php");
	class Blogcontrole{
		function inblog($blog){
			try{
                $conexao = new Conexao();

                $titulo = $blog->getTitulo();
                $autor = $blog->getAutor();
                $text = $blog->getTexto();
                $cmd =  $conexao->getConexao()->prepare("INSERT INTO blog(titulo,autor,text) VALUES(:titulo,:autor,:text);");
                $cmd->bindParam("titulo",$titulo);
                $cmd->bindParam("autor",$autor);
                $cmd->bindParam("text", $text);
                if($cmd->execute()){
                    $ultimo_id = $conexao->getConexao()->lastInsertId();
                    $conexao->fecharConexao();
                    return $ultimo_id;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro PDO: {$e->getMessage()}";
                return false;

            }catch(Exception $e){
                echo"Erro no Banco: {$e->getMessage()}";
                return false;
            }
        }
        
        function selecionar(){
            try{
                $conexao = new Conexao();
                $sql= $conexao->getConexao()->prepare("SELECT * FROM blog"); 
                $sql->execute();
                $result = $sql->fetchAll(PDO:: FETCH_CLASS, "Blogmodelo");
                return $result;       
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";

            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }

        function deletpost($id){
            try{
                $conexao = new Conexao();
                $com= $conexao->getConexao()->prepare("DELETE FROM blog WHERE id=:id");
                $com->bindParam("id",$id);
                if($com->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }

            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        
        function editar($blogg){
            try{
                $conexao = new Conexao();
                $blog = new Blogmodelo();
                $id = $blog->getId();
                $titulo = $blogg->getTitulo();
                $autor = $blogg->getAutor();
                $texto = $blogg->getTexto();
                $cmd = $conexao->getConexao()->prepare("UPDATE blog SET titulo=:titulo, texto=:texto WHERE autor=:autor");
                $cmd->bindParam("titulo", $titulo);
                $cmd->bindParam("autor", $autor);
                $cmd->bindParam("texto", $texto);
                if($cmd->execute()){
                        $conexao->fecharConexao();
                        return true;
                    }else{
                        $conexao->fecharConexao();
                        return false;
                    }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
    }
 ?>
