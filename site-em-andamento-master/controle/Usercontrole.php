<?php 
require_once("modelo/Usermodelo.php");
require_once("Conexao.php");
    class Usercontrole{
        function inserir($usuario){
            try{
                $conexao = new Conexao();
                $user = $usuario->getUser();
                $senha = $usuario->getSenha();
                $email = $usuario->getEmail();
                $cmd =  $conexao->getConexao()->prepare("INSERT INTO usuario(user,email,senha) VALUES(:user,:email,:senha);");
                $cmd->bindParam("user",$user);
                $cmd->bindParam("email",$email);
                $cmd->bindParam("senha", $senha);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro PDO: {$e->getMessage()}";
                return false;

            }catch(Exception $e){
                echo"Erro no Banco: {$e->getMessage()}";
                return false;
            }
        }
        function verificar($user,$senha){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM usuario WHERE user = :user AND senha =:senha");
                $cmd->bindParam("user", $user);
                $cmd->bindParam("senha", $senha);
                if($cmd->execute()){
                   if($cmd->rowCount()== 1){
                        return true;
                   }else{
                        return false;
                   }
                }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        /*function vemail($email){
            $conexao = new Conexao();
            $sql = "SELECT*FROM usuario WHERE email=:email";
            $verificar= $conexao->getConexao()->prepare($sql);
            $verificar->bindParam("email", $email);
            if($verificar->rowCount()>0){
                $conexao->fecharConexao();
                return true;
            }else{
                $conexao->fecharConexao();
                return false;
            }
        } 
        function vsenha($senha){
            $conexao = new Conexao();
            $sql ="SELECT*FROM usuario WHERE senha=:senha";
            $verificar = $conexao->getConexao()->prepare($sql);
            $verificar->bindParam("senha", $senha);
            if($verificar->rowCount()>0){
                $conexao->fecharConexao();
                return true;
            }else{
                $conexao->fecharConexao();
                return false;
            }
        }
		*/

        function update($usuarios){
            try{
                $conexao = new Conexao();
                $usuario = new Usermodelo();
                $user = $usuarios->getUser();
                $senha = $usuarios->getSenha();
                $cmd = $conexao->getConexao()->prepare("UPDATE usuario SET senha=:senha WHERE user=:user");
                $cmd->bindParam("user",$user);
                $cmd->bindParam("senha",$senha);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
     

    }
?>
