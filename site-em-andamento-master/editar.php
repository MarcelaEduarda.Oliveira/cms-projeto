<?php 
	try {
	    
		require_once("controle/Blogcontrole.php");
		require_once("modelo/Blogmodelo.php");
		
		$controle = new Blogcontrole();
		$blog = new Blogmodelo();
		
		$blog->setAutor($_POST['autor']);
		$blog->setTitulo($_POST['titulo']);
		$blog->setTexto($_POST['texto']);
		
		//Edita o post do usuário
		
		if($controle->editar($blog)){
		    
			header("Location: aduser.php");
		}
	} catch (Exception $e) {
		echo"Erro geral: {$e->getMessage()}";
	}

 ?>