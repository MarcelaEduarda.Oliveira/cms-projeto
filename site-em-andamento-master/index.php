
<?php include_once "nichos/head.php"; ?> <!--cabeçalho-->
</head>
<body>
	<?php include_once "nichos/menu.php"; ?> <!--menu-->
	<header>
		<img class='ui fluid image' src='img/banner.png'>
	</header>

	<!-- inicioconteudo -->

	<div class='ui vertical stripe segment'>
		<div class='ui middle aligned stackable grid container'>
			<div class='row'>
				<div class='eight wide column'>
					<h3 class='ui header'>Compartilhe suas idéias.</h3>
					<p>O Link Innovation garante que você tenha tudo que precisa para compartilhar suas idéias  hoje, pois possui um suporte de primeira classe, ferramentas de ajuda e muito mais.</p>
					<h3 class='ui header'>Crie qualquer tipo de post, para qualquer objetivo.</h3>
					<p>

						Com o Link Innovation, você cria posts que atende às suas necessidades. Com a sua otimização integrada e temas responsivos compatíveis com dispositivos móveis, não há limites para o que você pode alcançar com essa plataforma.
					</p>
				</div>
				<div class='six wide right floated column'>
					<img src='img/img1.jpg' class='ui large bordered rounded image'>
				</div>
			</div>
			<div class='row'>
				<div class='center aligned column'>
					<a class='ui huge button' href='cadastro.php' style='background-color: #080b34;color: white;'>Começar</a>
				</div>
			</div>
		</div>
	</div>


	<div class='ui vertical stripe quote segment'>
		<div class='ui equal width stackable internally celled grid'>
			<div class='center aligned row'>
				<div class='column'>
					<h3>Editor intuitivo</h3>
					<p>Nosso editor é rápido e intuitivo.</p>
				</div>
				<div class='column'>
					<h3>Blogs para celulares e computadores</h3>
					<p>
						Atualize seus posts em celulares ou em computadores: iOS, Android, Mac, Windows, e Linux.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class='ui vertical stripe segment'>
		<div class='ui text container'>
			<h3 class='ui header'>As pessoas amam o Link Innovation!</h3>

			<div class='ui comments'>
				<h4 class='ui dividing header'>Alguns comentários</h4>
				<div class='comment'>
					<a class='avatar'>
						<img src='img/av1.png'>
					</a>
					<div class='content'>
						<a class='author'>Matt</a>
						<div class='metadata'>
							<span class='date'>Hoje às 17:00</span>
						</div>
						<div class='text'>Que artístico! </div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
				</div>
				<div class='comment'>
					<a class='avatar'>
						<img src='img/av2.png'>
					</a>
					<div class='content'>
						<a class='author'>Mari</a>
						<div class='metadata'>
							<span class='date'>Ontem às 13:30</span>
						</div>
						<div class='text'>
							<p>Isto foi muito útil para a minha pesquisa. Muito obrigada!</p>
						</div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
					<div class='comments'>
						<div class='comment'>
							<a class='avatar'>
								<img src='img/av1.png'>
							</a>
							<div class='content'>
								<a class='author'>Marcos Silva</a>
								<div class='metadata'>
									<span class='date'>Agora</span>
								</div>
								<div class='text'>Mari, concordo com você!! :) </div>
								<div class='actions'>
									<a class='Resposta'>Resposta</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='comment'>
					<a class='avatar'>
						<img src='img/av2.png'>
					</a>
					<div class='content'>
						<a class='author'>Nay Maria</a>
						<div class='metadata'>
							<span class='date'>5 dias atrás</span>
						</div>
						<div class='text'>Cara, isso foi impressionante. Gostei muito! <3</div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
				</div>
			</div>

			<h4 class='ui horizontal header divider'>
				<a href='login.php'>Crie já o seu!</a>
			</h4>
			<h3 class='ui header'>Faça posts com segurança.</h3>
			<p>Com o Link Innovation, você tem muitas possibilidades de compartilhar seus posts de forma segura e intuitiva, além de ser gratuito. Então, o que você está esperando?</p>
			<center ><img src='img/icon.png' class='ui medium image'></center><br>

			<center><a class='ui large button' href='cadastro.php' style='background-color: #080b34;color: white;'>Venha conferir!</a></center>
		</div>
	</div>


	<!-- fimconteudo -->
	<?php include_once "nichos/footer.php"; ?>

</div> <!-- fim menu pusher -->
</body>
<?php include_once "nichos/scripts.php"; ?>
</html>