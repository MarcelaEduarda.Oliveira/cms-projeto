<?php
	require_once("controle/Usercontrole.php");
	
	session_start();
	
	$controle = new Usercontrole();
	
	$vrf = $controle->verificar($_POST['user'],$_POST['senha']);
	
	//Verifica se a sessão já existe e redireciona para a página de administração caso exista
	
	if(isset($_SESSION['user'])){
	    
		header("Location: aduser.php");
		
	}else{
	    //Caso a sessão não exista:
    	//Verifica se usuário está registrado  no banco e cria sessão
    	
    	if($vrf){
    		$_SESSION['user']=$_POST['user'];
    		$_SESSION['senha']=$_POST['senha'];
    		
    		//Redirecionamento para a página de administração 
    		
    		header("Location: aduser.php");
    		
    	}else{
    		header("Location: login.php");
    	}
	}
 ?>