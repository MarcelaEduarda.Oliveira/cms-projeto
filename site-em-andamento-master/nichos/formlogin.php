<form class='ui large form' action='nichos/../logar.php' method='post'>

	<div class='ui stacked segment'>
		<div class='field'>
			<div class='ui left icon input'>
				<i class='user icon'></i>
				<input type='text' name='user' placeholder='Usuário'>
			</div>
		</div>
		<div class='field'>
			<div class='ui left icon input'>
				<i class='lock icon'></i>
				<input type='password' name='senha' placeholder='Senha'>
			</div>
		</div>
		<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Login' />
	</div>

	<div class='ui error message'></div>

</form>