<?php
	session_start();
	if(isset($_SESSION['user'])){
		try{
			require_once("modelo/Imgmodelo.php");
			require_once("modelo/Blogmodelo.php");
			require_once("controle/Imgcontrole.php");
			require_once("controle/Blogcontrole.php");

			$control  = new Blogcontrole();
			$controle = new Imgcontrole();
			$blog     = new Blogmodelo();
			$imag     = new Imgmodelo();
			

			$blog->setTitulo($_POST['titulo']);
			$blog->setAutor($_POST['autor']);
			$blog->setTexto($_POST['texto']);

			
			$imagem=$_FILES['img'];
			$contagem = count($imagem['name']);

			//Verificar nome das imagens
			$nomes = [];
			for($i = 0; $i < $contagem; $i++){
				$veri = $controle->veri_nome($imagem['name'][$i]);

				if($veri){
					array_push($nomes, $imagem['name'][$i]);
				}

			}
			if(count($nomes) > 0){
				$_SESSION['erro'] = implode(", ", $nomes);
				header("Location: aduser.php");
				exit();
			}

			//Salvar blog e imagens
			$ultimo_id = $control->inBlog($blog);
			if($ultimo_id){
				if($contagem >= 1){
					for($i = 0; $i < $contagem; $i++){
						$imag->setNome($imagem['name'][$i]);
						$imag->setTipo($imagem['type'][$i]);
						$imag->setDados(file_get_contents($imagem['tmp_name'][$i]));
						$imag->setPost($ultimo_id);

						if($controle->inseririmg($imag)){
							header("Location: aduser.php");
						}
						
					}
				}

			}else{
				header("Location: aduser.php");
			}
		
		}catch(Exception $e){
			echo"Erro geral: {$e->Message()}";
		}
	}else{
		header("Location: index.php");
	}
 ?>
