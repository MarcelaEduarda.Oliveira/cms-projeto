<?php  
    session_start();
    if(isset($_SESSION['user'])){
        
        require_once("controle/Blogcontrole.php");
        require_once("controle/Imgcontrole.php");
        
        $controle = new Blogcontrole();
        $imgControl = new ImgControle();
        
        //Deleta a(s) imagem(ns) e o post do usuário
        
        if($imgControl->deletimg($_GET['id']) && $controle->deletpost($_GET['id'])){
            header("Location: aduser.php");
        }else{
            die();
        }
        
    }else{
        header("Location: index.php");
    }
?>
