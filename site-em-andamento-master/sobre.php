
<?php include_once "nichos/head.php"; ?> <!--cabeçalho-->
</head>
<body>
	<?php include_once "nichos/menu.php"; ?> <!--menu-->

	<header>
		<img class='ui fluid image'src='img/bannersobre.png'>
	</header>

	<!-- inicioconteudo -->

	<div class='ui vertical stripe segment'>
		<div class='ui middle aligned stackable grid container'>
			<div class='row'>
				<div class='eight wide column'>
					<h3 class='ui header'>Faça posts em minutos.</h3>
					<p>O Link Innovation garante que você tenha um desing criativo e moderno para o seus posts, com layout responsivo, ferramentas de administração e muito mais!</p>
					<h3 class='ui header'>Para quem foi criado?</h3>
					<p>

						O Link Innovation foi criado para que você  possa compartilhar e ver posts diferenciados,  sobre assuntos diversos. Veja posts que te mostre ideias novas, pensamentos novos, ou até sobre celebridades que você ame, e que tal fazer você mesmo um post ou varios posts sobre celebridades que ama?Cadastre-se e venha atingir novos horizontes!
					</p>
				</div>
				<div class='six wide right floated column'>
					<img src='img/icon.png' class='ui large bordered rounded image'>
				</div>
			</div>
			<div class='row'>
				<div class='center aligned column'>
					<a class='ui huge button' href='cadastro.php' style='background-color: #080b34;color: white;'>Começar</a>
				</div>
			</div>
		</div>
	</div>


	<div class='ui vertical stripe quote segment'>
		<div class='ui equal width stackable internally celled grid'>
			<div class='center aligned row'>
				<div class='column'>
					<h3>Editor intuitivo</h3>
					<p>Nosso editor é rápido e intuitivo.</p>
				</div>
				<div class='column'>
					<h3>Blogs para celulares e computadores</h3>
					<p>
						Atualize seu blog em celulares ou em computadores: iOS, Android, Mac, Windows, e Linux.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class='ui vertical stripe segment'>
		<div class='ui text container'>
			<h3 class='ui header'>As pessoas amam o Link Innovation!</h3>

			<div class='ui comments'>
				<h4 class='ui dividing header'>Alguns comentários</h4>
				<div class='comment'>
					<a class='avatar'>
						<img src='img/av1.png'>
					</a>
					<div class='content'>
						<a class='author'>Marcos</a>
						<div class='metadata'>
							<span class='date'>Hoje às 15:00</span>
						</div>
						<div class='text'>Fantástico! Melhor blog que já conheci!Indico super 3> </div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
				</div>
				<div class='comment'>
					<a class='avatar'>
						<img src='img/av2.png'>
					</a>
					<div class='content'>
						<a class='author'>Melissa</a>
						<div class='metadata'>
							<span class='date'>Ontem às 13:30</span>
						</div>
						<div class='text'>
							<p>Adorei os posts estou indo começar a fazer os meus posts! Muito obrigada!</p>
						</div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
					<div class='comments'>
						<div class='comment'>
							<a class='avatar'>
								<img src='img/av1.png'>
							</a>
							<div class='content'>
								<a class='author'>Roberto</a>
								<div class='metadata'>
									<span class='date'>Agora</span>
								</div>
								<div class='text'>Melissa, também estou indo fazer o meu pois amei tudo desse blog!! :) </div>
								<div class='actions'>
									<a class='Resposta'>Resposta</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='comment'>
					<a class='avatar'>
						<img src='img/av2.png'>
					</a>
					<div class='content'>
						<a class='author'>Nádia Maria</a>
						<div class='metadata'>
							<span class='date'>5 dias atrás</span>
						</div>
						<div class='text'>Cara, isso é impressionante. Amei! <3</div>
						<div class='actions'>
							<a class='Resposta'>Resposta</a>
						</div>
					</div>
				</div>
			</div>

			<h4 class='ui horizontal header divider'>
				<a href='cadastro.php'>Vamos começar?</a>
			</h4>
			<h3 class='ui header'>Crie um blog com segurança.</h3>
			<p>Com o Link Innovation, você tem muitas possibilidades de criar seus blogs de forma segura e intuitiva, além de ser gratuito. Então, o que você está esperando?</p>
			<center><a class='ui large button' href='cadastro.php' style='background-color: #080b34;color: white;'>Venha conferir!</a></center>
		</div>
	</div>


	<!-- fimconteudo -->

	<?php include_once "nichos/footer.php"; ?>

</div> <!-- fim menu pusher -->
</body>
<?php include_once "nichos/scripts.php"; ?>
</html>